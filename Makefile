PROJECT_NAME ?= $(shell basename $(CURDIR))

TARGET_NAME ?= array-test

TMP_DIR ?= /tmp/$(PROJECT_NAME)

PREFIX ?= $(TMP_DIR)

SRC_DIR ?= .

CFLAGS += -Wall -g

CFLAGS += -I./src -I./test

CFLAGS += -MMD -MP

C_FILES = $(shell find $(SRC_DIR) -name '*.c')

O_FILES = $(C_FILES:$(SRC_DIR)/%.c=$(TMP_DIR)/%.o)

D_FILES = $(O_FILES:%.o=%.c)

LD = gcc

TARGET = $(PREFIX)/$(TARGET_NAME)

all: $(TARGET)

$(TARGET): $(O_FILES)
	@mkdir -p $(shell dirname $@)
	$(LD) $(LDLIBS) $(O_FILES) -o $(TARGET)

run: all
	$(TARGET)

vg: all
	valgrind ./$(TARGET)

$(TMP_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(shell dirname $@)
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f $(O_FILES) $(D_FILES)

-include $(D_FILES)
