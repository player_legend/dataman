			       _________

				DATAMAN
			       _________


Table of Contents
_________________

1 Description
2 Useage
3 Documentation (currently incomplete)
4 TODO 





1 Description
=============

  This is a basic toolset for managing collections of data, such as
  pointers and various metadata associated with them.


2 Useage
========

  These files are intended to be statically compiled into a project. You
  will not need test.c and benchmark.h, those are just for testing
  purposes.


3 Documentation (currently incomplete)
======================================

  See the 'docs' directory for html documentation and *.org* files for
  use with emacs.


4 TODO 
=======

  document and comment
