#+OPTIONS: ^:nil
#+OPTIONS: _:nil

#+TITLE: dataman-da-tree.h

* About

  This is a guide to the *dataman-da-tree.h* header file.
  For general information about *dataman* library, see the [[./dataman.org][dataman.h]]
  documentation.
  
  *dataman-da-tree.h* implements tree traversal functionality for the
  *dab* and *data_array* types described in [[./dataman-da.org][dataman-da.h]].

* Includes

  [[./dataman-da.org][dataman-da.h]]

* Types

  - *enum tree_action*

    An enumerated type that the *data_node_f* callback returns, it is used
    to modify the behavior of the *da__walk* function from within node
    callbacks.

    - *TA__CONT*

      Continue traversal normally, following the path of
      
      pre-node func -> child func for each child -> post-node func

    - *TA__UP*

      Go to the current entry's parent next.

    - *TA__NO_DESCEND*

      Do not descend at this entry.

    - *TA__REPEAT*

      Repeat this function.

    - *TA__NODE_PARENT*

      Add another parent to this entry such that the new parent replaces
      this entry in its current parent.

    - *TA__RAISE_CHILDREN*

      Delete this entry and replace in it its parent with its children.
      If the current entry is not a node, the traversal will continue as
      though *TA__CONT* was returned.

    - *TA__DATA_BEFORE* / *TA__DATA_AFTER* / *TA__NODE_BEFORE* / *TA__NODE_AFTER*

      Create a data or node either before or after the current and step to it next.

    - *TA__DATA_PREV* / *TA__DATA_NEXT* / *TA__NODE_PREV* / *TA__NODE_NEXT*

      Behaves like *TA__DATA_BEFORE* / *TA__DATA_AFTER* / *TA__NODE_BEFORE* / *TA__NODE_AFTER*,
      but takes into account whether or not traversal is reversed. For example,
      whether or not traversal is reversed, *TA__DATA_NEXT* will insert data
      such that it would normally be stepped to next, *TA__DATA_PREV* would insert data
      such that the traversal function would step back.

    - *TA__DELETE* / *TA__FREE_DELETE*

      Delete the current entry from the parent by shifting all members after
      it down by one. If the current node is a parent, *TA__DELETE* will
      leave its data in place, and 

  - *node_arg*

    
