#+OPTIONS: ^:nil
#+OPTIONS: _:nil

#+TITLE: dataman-ht.h

* About

  This is a guide to the *dataman-ht.h* header file.
  For general information about *dataman* library, see the [[./dataman.org][dataman.h]]
  documentation.
  
  *dataman-ht.h* implements a *hash table* which may be configured as
  ordered or disordered. Ordered lookups implement a binary search, and
  so are much faster than disordered lookups.

* Includes
  
  [[./dataman-defs.org][dataman-defs.h]]

* Type Definitions

  - *struct hash_table*

    The hash table structure

    Members are:

    - *.pa*

      A [[./dataman-pa.org][ptr_array]] which contains the hash values stored in the table.

    - *.is_disordered*
      
      A boolean value which indicates that the hash table should be used
      as ordered or disordered.

* Functions

  

  - *hash_t ht__gen_hash( const void * data, size_t size )*

    Generates a hash from data. *data* is a pointer to the data and
    *size* is the size of the data.

  - *void ht__insert( struct hash_table * table, index_t index, hash_t hash )*

    Inserts a value directly at *index*. Can break ordering if used
    incorrectly.

  - *bool ht__find( const struct hash_table * table, hash_t find_hash, index_t * index )*

    Attempts to find a value to match *find_hash* in *table*.
    *index* will be filled with either the located value's index, or
    a suitable position to insert the desired hash if it is not
    already in the table.

    If the table is not ordered and the hash is not found, *index*
    will be set to one more than the greatest index in the table.
    
    This function returns *true* if the hash was found and *false* if
    it was not found.

  - *bool ht__add( struct hash_table * table, hash_t find_hash, bool dupe, index_t * index )*

    Attempts to add *add_hash* to *table*.

    If the table is not configured as ordered, *add_hash* will be added
    to the end of the table.

    If *dupe* is true, then the hash value will be duplicated in the array,
    if it already exists. If it does not exist, then it will be added
    normally.

    This function returns *true* if a new value was added to the array, and
    *false* if it was not.

  - *void ht__clear( struct hash_table * table, bool free_alloc )*

    Clears the hash table, if *free_alloc* is true, then the data
    allocated to the array is freed.

  - *bool ht__delete( struct hash_table * table, index_t index )*

    Delete an index in the table by shifting all entries above *index*
    down by one.

    This function returns true if a deletion was performed, and false if
    the array was empty.
