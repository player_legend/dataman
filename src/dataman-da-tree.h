//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-da-tree.h

    Tree functionality for the data array type
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_DA_TREE_H
#define DATAMAN_DA_TREE_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman-da.h"

//==============================================================================
//    Type definitions
//==============================================================================

enum tree_action
  {
    TA__CONT,
    TA__UP,
    TA__NO_DESCEND,
    TA__REPEAT,

    TA__NODE_PARENT,
    TA__RAISE_CHILDREN,
    
    TA__DATA_BEFORE,
    TA__DATA_AFTER,
    TA__NODE_BEFORE,
    TA__NODE_AFTER,

    TA__DATA_PREV,
    TA__DATA_NEXT,
    TA__NODE_PREV,
    TA__NODE_NEXT,

    TA__DELETE,
    TA__FREE_DELETE,
  };

typedef struct
{
  struct {
    union {
      struct data_array * node;
      dab * data;
    };
    struct data_array * parent;
  } ptr;
  struct {
    index_t depth;
    index_t index;
    index_t parent_count;
  } pos;
  void * data_arg;
  bool is_new;
  bool parent_pre;
  bool parent_post;
} node_arg;

typedef enum tree_action (*data_node_f)( const node_arg arg );

typedef struct
{
  void * arg;
  bool reverse;
  index_t current_depth;
  data_node_f leaf_func;
  data_node_f pre_node_func;
  data_node_f post_node_func;
} da_walk_options;

//==============================================================================
//    Functions
//==============================================================================

struct data_array * da__make_node( struct data_array * restrict array, index_t index );
void da__walk( struct data_array * restrict root, da_walk_options * options );

//==============================================================================

#endif // #ifndef DATAMAN_DA_TREE_H
