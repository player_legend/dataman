//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-da.h

    Data array definitions
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_DA_H
#define DATAMAN_DA_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman-defs.h"

//==============================================================================
//    Type definitions
//==============================================================================

struct data_array
{
  struct data_buffer * entry;
  struct {
    index_t count;
    enum array_flags flags;
    struct data_array * parent;
  } info;
};

typedef struct data_buffer
{
  struct {
    type_id_t main;
    union {
      type_id_t flags;
      type_id_t sub;
    };
  } type;
  union {
    struct data_array node; // branch
    struct { // leaf
      free_f free;
      union {
	size_t size;
	index_t flags;
      };
      union {
	void * data;
	char * string;
	intptr_t sint;
	uintptr_t uint;
      };
    };
  };
} dab;

//==============================================================================
//    Functions
//==============================================================================

dab * da__push( struct data_array * array );

dab * da__expand( struct data_array * restrict array, index_t index ); // index greater than array count

dab * da__insert( struct data_array * restrict array, index_t index ); // index less than or equal to array count

dab * da__add( struct data_array * restrict array, index_t index ); // any index

dab da__pop( struct data_array * restrict array );

void da__clear( struct data_array * restrict array, bool free_alloc, bool free_data );

void da__clear_entry( dab * entry, bool free_data );

bool da__delete( struct data_array * restrict array, index_t index, bool maintain_order, bool free_data );

void da__copy_range( struct data_array * dst, struct data_array * src, index_t dst_start, index_t src_start, index_t size );

//==============================================================================

#endif // #ifndef DATAMAN_DA_H
