
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-defs.h

    General data management definitions
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_DEFS_H
#define DATAMAN_DEFS_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "base-flags.h"
#include "base-index.h"

//
//    System
//

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

//==============================================================================
//    Type definitions
//==============================================================================

typedef void (*free_f)( void * free_target ); // callback for free-like functions

//    so we don't accidentally collide with enums, make the data node type the max value of type_id_t
#define TYPE__DATA_NODE TYPE_ID_MAX

enum array_flags
  {
    A_DID_REALLOC = FL(0),
    A_DID_RESIZE = FL(1),
    A_IS_TREE = FL(2),
  };

//==============================================================================
//    Allocation / generic array modification
//==============================================================================

// AFTER -- these two macros are run after the count has been decremented
#define ARRAY_FLIP_DELETE_AFTER( array, count, index ) array[index] = array[count];
#define ARRAY_SHIFT_DELETE_AFTER( array, count, index ) memmove( array + index, array + index + 1, ((count) - index)*sizeof( *array ) );

//    hail satan
#define ALLOC_MASK 0666666666
#define ALLOC_RATE_SHIFT 3
#define ALLOC_RATE (1 << ALLOC_RATE_SHIFT)
#define NEW_ALLOC_COUNT( new_count ) ((new_count)*ALLOC_RATE)
#define GET_ALLOC_COUNT( count, alloc_count ) { for( alloc_count = ALLOC_RATE; alloc_count <= count; alloc_count <<= ALLOC_RATE_SHIFT ); }

#define INCREMENT_DID_REALLOC( count ) (((count) & (ALLOC_MASK | ((count) - 1))) == 0)
#define INCREMENT_ARRAY( array )				\
  (array).info.count++;						\
  (array).info.flags |= A_DID_RESIZE;			\
  SET_FLAG_BITS( (array).info.flags,				\
		 A_DID_REALLOC,				\
		 INCREMENT_DID_REALLOC( (array).info.count ) );

//==============================================================================
//    Generic subroutine functions
//==============================================================================

void * array_insert_shift( void * array, index_t index, index_t count, size_t member_size, enum array_flags flags );
void * array_expand( void * array, size_t member_size, index_t count, index_t new_count, enum array_flags * restrict flags );

//==============================================================================

#endif // #ifndef DATAMAN_DEFS_H
