//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-dt.h

    Hash indexed data table
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_DT_H
#define DATAMAN_DT_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman-ht.h"
#include "dataman-da.h"

//==============================================================================
//    Type definitions
//==============================================================================

enum dt_action
  {
    DT__NEW,  // return NULL if the entry exists
    DT__OVER, // if the entry exists, return it. if it doesn't, create another
    DT__DUPE, // if the entry exists, create a duplicate of it, if it doesn't, create a new entry
  };

struct data_table
{
  struct hash_table ht;
  dab * entry;
};

//==============================================================================
//    Functions
//==============================================================================

dab * dt__insert( struct data_table * table, index_t index, hash_t hash );
dab * dt__add( struct data_table * table, hash_t hash, enum dt_action action );
dab * dt__find( const struct data_table * table, hash_t hash );
index_t dt__index( struct data_table * table, dab * entry );
void dt__clear( struct data_table * table, bool free_alloc, bool free_data );
bool dt__delete( struct data_table * table, index_t index, bool free_data );

//==============================================================================

#endif // #ifndef DATAMAN_DT_H
