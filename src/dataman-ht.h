//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-ht.h

    Hash table definitions
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_HT_H
#define DATAMAN_HT_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman-pa.h"

//==============================================================================
//    Type definitions
//==============================================================================

struct hash_table
{
  struct ptr_array pa;
  bool is_disordered;
};

//==============================================================================
//    Function definitions
//==============================================================================

hash_t ht__gen_hash( const void * data, size_t size );

void ht__insert( struct hash_table * table, index_t index, hash_t hash );

bool ht__find( const struct hash_table * table, hash_t find_hash, index_t * index ); // true if found, if not found then the index will be a suitable position to add a new member

bool ht__add( struct hash_table * table, hash_t add_hash, bool dupe, index_t * index ); // true if added new

void ht__clear( struct hash_table * table, bool free_alloc );

bool ht__delete( struct hash_table * table, index_t index );

//==============================================================================

#endif // #ifndef DATAMAN_HT_H
