//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-list.h

    Double linked lists
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_LIST2_H
#define DATAMAN_LIST2_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman-da.h"
#include "dataman-defs.h"

//==============================================================================
//    Type definitions
//==============================================================================

struct list2_member{ // this doesn't have a data pointer, rather, you should embed it in a struct
  struct {
    struct list2_member * previous;
    struct list2_member * next;
    struct list2 * parent;
  } link;

  struct list2_member_info{
    free_f free;
    type_id_t type;
  } info;
};

typedef struct list2_member_info l2_mem_info;

struct list2{
  struct list2_member member;
  struct list2_member * first;
  struct list2_member * last;
  struct data_array parking;
  index_t count;
};

//==============================================================================
//    Macros
//==============================================================================

// calculates the next member before the cycle so it's safe to unlink the list_member in the loop
#define list2__FOR_EACH_LINK( list_parent, list_member )	\
  for( struct list2_member * internal_next = (void*)(list_parent).first;	\
       (NULL != ((list_member) = (void*)internal_next)) && (internal_next = internal_next->link.next, 1); \
       )

//==============================================================================
//    Functions
//==============================================================================

void list2__unlink( void * target_member_p, bool free_target_member );

void list2__clear( struct list2 * target, bool free_data );

void list2__park( struct list2 * target, size_t size, void * target_member_p );

void * list2__new( struct list2 * target, size_t size );

struct list2_member * list2__add_after( struct list2 * target, void * target_member_p, void * compatible_struct );

struct list2_member * list2__add_before( struct list2 * target, void * target_member_p, void * compatible_struct );

struct list2_member * list2__append( struct list2 * target, void * compatible_struct );

struct list2_member * list2__prepend( struct list2 * target, void * compatible_struct );

//==============================================================================

#endif // #ifndef DATAMAN_LIST2_H
