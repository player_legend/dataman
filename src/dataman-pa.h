//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman-pa.h

    Pointer array definitions
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DATAMAN_PA_H
#define DATAMAN_PA_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman-defs.h"

//==============================================================================
//    Typedefs
//==============================================================================

typedef union {
    void * data;
    char * string;
    intptr_t sint;
    uintptr_t uint;
    hash_t hash;
} pa_entry; // pointer array entry

struct ptr_array
{
  pa_entry * entry;
  
  struct
  {
    index_t count;
    enum array_flags flags;
  } info;
};

//==============================================================================
//    Functions
//==============================================================================

pa_entry * pa__push( struct ptr_array * array ); // index equal to array count

pa_entry * pa__expand( struct ptr_array * array, index_t index ); // index greater than array count

pa_entry * pa__insert( struct ptr_array * array, index_t index ); // index less than or equal to array count

pa_entry * pa__add( struct ptr_array * array, index_t index ); // any index

pa_entry pa__pop( struct ptr_array * array );

void pa__clear( struct ptr_array * array, bool free_alloc, free_f data_free );

bool pa__delete( struct ptr_array * array, index_t index, bool maintain_order );

//==============================================================================

#endif // #ifndef DATAMAN_PA_H
