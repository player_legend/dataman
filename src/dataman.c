//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    dataman.c

    Data management functions
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

//==============================================================================
//    Includes
//==============================================================================

//
//    Local
//

#include "dataman.h"

//
//    System
//

#include <string.h>

//
//    Debug includes
//

#include <stdio.h>

//==============================================================================
//    Generic array subroutines
//==============================================================================

void * array_insert_shift( void * array, index_t index, index_t count, size_t member_size, enum array_flags flags )
//    run after the count has been incremented
{
  size_t lower_copy = index * member_size;
  size_t upper_copy = count * member_size - lower_copy;
  size_t shift_offset = lower_copy + member_size;

  if( flags & A_DID_REALLOC )
    {
      void * ret = malloc( member_size * NEW_ALLOC_COUNT( count ) );
      if( array != NULL )
	{
	  memcpy( ret, array, lower_copy );
	  memcpy( ret + shift_offset, array + lower_copy, upper_copy );
	}

      free( array );
      return ret;
    }
  else
    {
      memmove( array + shift_offset, array + lower_copy, upper_copy );
      return array;
    }
}

void * array_expand( void * array, size_t member_size, index_t count, index_t new_count, enum array_flags * restrict flags )
{
  index_t alloc_count;

  GET_ALLOC_COUNT( count, alloc_count );

  if( alloc_count < new_count || count < alloc_count )
    {
      GET_ALLOC_COUNT( new_count, alloc_count );

      array = realloc( array, alloc_count * member_size );
      *flags |= A_DID_REALLOC;
    }
  else
    {
      *flags &= ~A_DID_REALLOC;
    }

  memset( array + (count * member_size), 0, (new_count - count) * member_size );

  return array;
}

//==============================================================================
//    Pointer array
//==============================================================================

#ifndef __DATAMAN_NO_PA

pa_entry * pa__push( struct ptr_array * restrict array ) // index equal to array count
{
  index_t new_index = array->info.count;

  INCREMENT_ARRAY( *array );
  
  if( array->info.flags & A_DID_REALLOC )
    {
      array->entry = realloc( array->entry, NEW_ALLOC_COUNT( array->info.count ) * sizeof( *array->entry ) );
    }

  return array->entry + new_index;
}

pa_entry * pa__expand( struct ptr_array * restrict array, index_t index ) // index greater than array count
{
  index_t new_count = index + 1;
  array->info.flags |= A_DID_RESIZE;
  array->entry = array_expand( array->entry, sizeof( *array->entry ), array->info.count, new_count, &array->info.flags );
  array->info.count = new_count;
  return array->entry + index;
}

pa_entry * pa__insert( struct ptr_array * restrict array, index_t index ) // index less than or equal to array count
{
  INCREMENT_ARRAY( *array );
  array->entry = array_insert_shift( array->entry, index, array->info.count, sizeof( *array->entry ), array->info.flags );
  return array->entry + index;
}

pa_entry * pa__add( struct ptr_array * restrict array, index_t index ) // any index
{
  if( index < array->info.count )
    {
      return pa__insert( array, index );
    }
  else if( index > array->info.count )
    {
      return pa__expand( array, index );
    }
  else
    {
      return pa__push( array );
    }
}

pa_entry pa__pop( struct ptr_array * restrict array )
{
  if( array->info.count == 0 )
    {
      return (pa_entry){};
    }
  
  array->info.count--;
  
  return array->entry[array->info.count];
}

void pa__clear( struct ptr_array * restrict array, bool free_alloc, free_f data_free )
{
  if( data_free )
    {
      for( index_t i = 0; i < array->info.count; i++ )
	{
	  data_free( array->entry[i].data );
	}
    }

  if( free_alloc )
    {
      free( array->entry );
      *array = (struct ptr_array){};
    }
  else
    {
      array->info.count = 0;
    }
}

bool pa__delete( struct ptr_array * restrict array, index_t index, bool maintain_order )
{
  if( index >= array->info.count )
    {
      return false;
    }
  
  array->info.count--;
  
  if( maintain_order )
    {
      ARRAY_SHIFT_DELETE_AFTER( array->entry, array->info.count, index );
    }
  else
    {
      ARRAY_FLIP_DELETE_AFTER( array->entry, array->info.count, index );
    }

  return true;
}
#endif // #ifndef __DATAMAN_NO_PA

//==============================================================================
//    Data array tree
//==============================================================================

#ifndef __DATAMAN_NO_DA_TREE

static void da__fix_child_backreferences( struct data_array * node )
{
  dab * child;
  for( index_t i = 0; i < node->info.count; i++ )
    {
      child = node->entry + i;

      if( child->type.main == TYPE__DATA_NODE )
	{
	  child->node.info.parent = node;
	}
    }
}

static void da__fix_backreferences( struct data_array * node, index_t start_index )
{
  dab * child;
  for( index_t i = start_index; i < node->info.count; i++ )
    {
      child = node->entry + i;

      if( child->type.main == TYPE__DATA_NODE )
	{
	  da__fix_child_backreferences( &child->node );
	  child->node.info.parent = node;
	}
    }
}

struct da_walk_state
{
  struct ptr_array index_stack;
  index_t index;
  bool new_entry;
  int increment;
  bool no_post;
  bool node_ended;
  struct data_array * head;
  dab * data;
  struct data_array * node;
  struct data_array * root;
};

struct data_array * da__make_node( struct data_array * restrict array, index_t index )
{
  array->info.flags |= A_IS_TREE;
  array->entry[index] = (dab){ .type.main = TYPE__DATA_NODE, .node.info.parent = array };
  return &array->entry[index].node;
}

#define GO_FORWARD( s ) (s).index += (s).increment
#define GET_DATA( s ) ((s).data = (s).head->entry + (s).index)
#define HEAD_IS_VALID( s ) ((s).index_stack.info.count > 0)
#define GET_NODE( s ) ((s).node = HEAD_IS_VALID( s ) ? &(s).head->entry[(s).index].node : (s).root)
#define DATA_IS_NODE( s ) (HEAD_IS_VALID( s ) ? GET_DATA( s )->type.main == TYPE__DATA_NODE : true)
#define HEAD_COUNT( s ) (HEAD_IS_VALID( s ) ? (s).head->info.count : 1)

#define DEBUG_NULL_HEAD( msg, s ) printf( msg " root %p head %p at %u\n", (s).root, (s).head, (s).index_stack.info.count ); fflush( stdout )

static bool da__mod_action( struct da_walk_state * s, enum tree_action action ) // return true to retry the current node, false to run TA__CONT
{
  if( s->head == NULL )
    {
      return false;
    }
  
  switch( action )
    {
    case TA__DATA_NEXT:
      action = s->increment > 0 ? TA__DATA_AFTER : TA__DATA_BEFORE;
      break;
      
    case TA__DATA_PREV:
      action = s->increment > 0 ? TA__DATA_BEFORE : TA__DATA_AFTER;
      break;
      
    case TA__NODE_NEXT:
      action = s->increment > 0 ? TA__NODE_AFTER : TA__NODE_BEFORE;
      break;
      
    case TA__NODE_PREV:
      action = s->increment > 0 ? TA__NODE_BEFORE : TA__NODE_AFTER;
      break;
      
    default:
      break;
    }
  
  switch( action )
    {
    case TA__NODE_AFTER:
    case TA__DATA_AFTER:
      s->index += 1; // this might be okay regardless of increment value

    case TA__NODE_BEFORE:
    case TA__DATA_BEFORE:
      s->new_entry = true;

      *da__insert( s->head, s->index ) = (dab){};
      if( action == TA__NODE_AFTER || action == TA__NODE_BEFORE )
	{
	  da__make_node( s->head, s->index );
	}
      return true;
      
    case TA__NODE_PARENT:
      s->new_entry = true;
      {
	GET_DATA( *s );
	dab tmp_data = *s->data;
	struct data_array * new_node;
	dab * new_data;
	new_node = da__make_node( s->head, s->index );
	new_data = da__push( &s->data->node );
	*new_data = tmp_data;
	if( tmp_data.type.main == TYPE__DATA_NODE )
	  {
	    new_data->node.info.parent = new_node;
	    da__fix_child_backreferences( &new_data->node );
	  }
      }
      return true;
	 
    case TA__RAISE_CHILDREN:
      if( GET_DATA( *s )->type.main != TYPE__DATA_NODE )
	{
	  GO_FORWARD( *s );
	  return false;
	}

      GET_NODE( *s );
      da__copy_range( s->head, s->node, s->index + 1, 0, s->node->info.count );
      
    case TA__FREE_DELETE:
    case TA__DELETE:
      
      da__delete( s->head, s->index, true, action == TA__FREE_DELETE );

      if( s->increment < 0 )
	{
	  s->index -= 1;
	}
      
      return true;
      
    default:
      return true;
    }
}

static bool head_should_change( struct da_walk_state * s )
{
  if( s->index >= HEAD_COUNT( *s ) )
    {
      s->node_ended = true;
      
      return true;
    }
  else
    {
      s->node_ended = false;
      
      return false;
    }
  
}

static void go_down( struct da_walk_state * s, da_walk_options * options )
{
  s->head = GET_NODE( *s );
  pa__push( &s->index_stack )->uint = s->index;
  s->index = options->reverse ? s->head->info.count - 1 : 0;
}

static bool go_up( struct da_walk_state * s )
{ 
  s->index = pa__pop( &s->index_stack ).uint;

  if( s->index_stack.info.count == 0 )
    {
      return false;
    }
  else
    {
      s->head = s->head->info.parent;
      return true;
    }
}

static bool start_node( struct da_walk_state * s, da_walk_options * options ) // return true to descend, false to remain
{
  enum tree_action action;
  
 RESTART:
  
  if( head_should_change( s ) || !DATA_IS_NODE( *s ) )
    {
      s->no_post = true;
      return false;
    }
  
  GET_NODE( *s );
  
  s->no_post = false;
      
  if( options->pre_node_func )
    {
      action = options->pre_node_func
	( (node_arg){
	  .ptr = { .node = s->node, .parent = s->head },
	    .pos = { s->index_stack.info.count, s->index, HEAD_COUNT( *s ) },
	    options->arg, s->new_entry, true, false } );
    }
  else
    {
      action = TA__CONT;
    }
  
  s->new_entry = false;

  switch( action )
    {
    case TA__REPEAT:
      goto RESTART;
      
    default:
      if( da__mod_action( s, action ) )
	{
	  goto RESTART;
	}
    case TA__CONT:
      return true;

    case TA__UP:
    case TA__NO_DESCEND:
      return false;
    }
}

static bool leaf_node( struct da_walk_state * s, da_walk_options * options ) // return true to go forward, return false to break
{
  enum tree_action action;

 RESTART:
  
  if( head_should_change( s ) || DATA_IS_NODE( *s ) )
    {
      return false;
    }
  
  GET_DATA( *s );
  
  if( options->leaf_func )
    {
      action = options->leaf_func
	( (node_arg){
	  .ptr = { .data = s->data, .parent = s->head },
	    .pos = { s->index_stack.info.count, s->index, s->head->info.count },
	    options->arg, s->new_entry, false, false } );
    }
  else
    {
      action = TA__CONT;
    }
  
  s->new_entry = false;
  
  switch( action )
    {
    case TA__REPEAT:
      goto RESTART;
      
    default:
      if( da__mod_action( s, action ) )
	{
	  goto RESTART;
	}
    case TA__NO_DESCEND:
    case TA__CONT:
      return true;

    case TA__UP:
      return false;
    }
  
  s->no_post = false;
}

static void end_node( struct da_walk_state * s, da_walk_options * options )
{
  if( s->no_post )
    {
      return;
    }
    
  enum tree_action action;

 RESTART:
  
  if( s->index >= HEAD_COUNT( *s ) || !DATA_IS_NODE( *s ) )
    {
      return;
    }
  
  GET_NODE( *s );
  
  if( options->post_node_func )
    {
      action = options->post_node_func
	( (node_arg){
	  .ptr = { .node = s->node, .parent = s->head },
	    .pos = { s->index_stack.info.count, s->index, HEAD_COUNT( *s ) },
	    options->arg, s->new_entry, false, true } );
    }
  else
    {
      action = TA__CONT;
    }
  
  s->new_entry = false;
    
  switch( action )
    {
    case TA__REPEAT:
      goto RESTART;
      
    default:
      if( da__mod_action( s, action ) )
	{
	  goto RESTART;
	}
    case TA__CONT:
    case TA__UP:
    case TA__NO_DESCEND:
      break;
    }
  
  s->no_post = false;
}

void da__walk( struct data_array * restrict root, da_walk_options * options )
{
  struct da_walk_state s =
    {
      .increment = options->reverse ? -1 : 1,
      .root = root,
    };
  
  if( start_node( &s, options ) )
    {
      go_down( &s, options );
      while( HEAD_IS_VALID( s ) )
	{
	  while( leaf_node( &s, options ) )
	    {
	      GO_FORWARD( s );
	    }
	  
	  if( s.node_ended )
	    {
	      go_up( &s );
	      end_node( &s, options );
	      GO_FORWARD( s );
	    }
	  else if( start_node( &s, options ) )
	    {
	      go_down( &s, options );
	    }
	  else
	    {
	      end_node( &s, options );
	      GO_FORWARD( s );
	    }
	}
    }
}

#else

//    empty macro
#define da__fix_backreferences( node, start_index )

#endif // #ifndef __DATAMAN_NO_DA_TREE

//==============================================================================
//    Data array
//==============================================================================

#ifndef __DATAMAN_NO_DA

dab * da__push( struct data_array * array )
{
  index_t new_index = array->info.count;

  INCREMENT_ARRAY( *array );
  
  if( array->info.flags & A_DID_REALLOC )
    {
      array->entry = realloc( array->entry, NEW_ALLOC_COUNT( array->info.count ) * sizeof( *array->entry ) );
      array->entry[new_index] = (dab){};
      
      if( array->info.flags & A_IS_TREE )
	{
	  da__fix_backreferences( array, 0 );
	}
    }
    
  return array->entry + new_index;
}

dab * da__expand( struct data_array * restrict array, index_t index ) // index greater than array count
{
  index_t new_count = index + 1;
  array->info.flags |= A_DID_RESIZE;
  array->entry = array_expand( array->entry, sizeof( *array->entry ), array->info.count, new_count, &array->info.flags );
  array->info.count = new_count;

  if( array->info.flags & A_DID_REALLOC & A_IS_TREE )
    {
      da__fix_backreferences( array, 0 );
    }
  
  return array->entry + index;
}

dab * da__insert( struct data_array * restrict array, index_t index ) // index less than or equal to array count
{
  INCREMENT_ARRAY( *array );
  array->entry = array_insert_shift( array->entry, index, array->info.count, sizeof( *array->entry ), array->info.flags );
  
  if( array->info.flags & A_IS_TREE )
    {
      if( array->info.flags & A_DID_REALLOC )
	{
	  da__fix_backreferences( array, 0 );
	}
      else
	{
	  da__fix_backreferences( array, index );
	}
    }
  
  return array->entry + index;
}

dab * da__add( struct data_array * restrict array, index_t index ) // any index
{
  if( index < array->info.count )
    {
      return da__insert( array, index );
    }
  else if( index > array->info.count )
    {
      return da__expand( array, index );
    }
  else
    {
      return da__push( array );
    }
}

dab da__pop( struct data_array * restrict array )
{
  if( array->info.count == 0 )
    {
      return (dab){};
    }
  
  array->info.count--;
  
  return array->entry[array->info.count];
}

#ifndef __DATAMAN_NO_DA_TREE
static enum tree_action clearnode__post_node_func( node_arg arg )
{
  free( arg.ptr.node->entry );
  
  return TA__CONT;
}

#warning todo
static enum tree_action clearnode__data_func( node_arg arg )
{
  if( arg.ptr.data->free && arg.ptr.data->data )
    {
      arg.ptr.data->free( arg.ptr.data->data );
    }
  
  return TA__CONT;
}

void da__clear_entry( dab * entry, bool free_data )
{
  if( entry->type.main == TYPE__DATA_NODE )
    {
      da__walk( &entry->node, &(da_walk_options){
	  .leaf_func = free_data ? clearnode__data_func : NULL,
	    .post_node_func = clearnode__post_node_func } );
    }
  else if( free_data && entry->free && entry->data )
    {
      entry->free( entry->data );
    }
}
#else
void da__clear_entry( dab * entry, bool free_data )
{
  if( free_data && entry->free && entry->data )
    {
      entry->free( entry->data );
    }
}
#endif

void da__clear( struct data_array * restrict array, bool free_alloc, bool free_data )
{
  if( free_data )
    {
      for( index_t i = 0; i < array->info.count; i++ )
	{
	  da__clear_entry( array->entry + i, true );
	}
    }

  if( free_alloc )
    {
      free( array->entry );
      *array = (struct data_array){};
    }
  else
    {
      array->info.count = 0;
    }
}

bool da__delete( struct data_array * restrict array, index_t index, bool maintain_order, bool free_data )
{
  if( index >= array->info.count )
    {
      return false;
    }

  da__clear_entry( array->entry + index, free_data );
  
  array->info.count--;

  if( index < array->info.count )
    {
      if( maintain_order )
	{
	  ARRAY_SHIFT_DELETE_AFTER( array->entry, array->info.count, index );
	}
      else
	{
	  ARRAY_FLIP_DELETE_AFTER( array->entry, array->info.count, index );
	}
    }

  return true;
}

 
void da__copy_range( struct data_array * dst, struct data_array * src, index_t dst_start, index_t src_start, index_t size )
{
  index_t shift_offset = dst_start + size;
  index_t new_dst_count = dst->info.count + size;

  struct data_array src_tmp = *src;
  
  dst->entry = array_expand( dst->entry, sizeof( *dst->entry ), dst->info.count, new_dst_count, &dst->info.flags );

  index_t upper_shift = dst->info.count - dst_start;
  
  memmove( dst->entry + shift_offset, dst->entry + dst_start, upper_shift * sizeof( *dst->entry ) );

  memcpy( dst->entry + dst_start, src_tmp.entry + src_start, size * sizeof( *dst->entry ) );

  dst->info.count = new_dst_count;

#ifndef __DATAMAN_NO_DA_TREE
  if( dst->info.flags & A_IS_TREE )
    {
      if( dst->info.flags & A_DID_REALLOC )
	{
	  da__fix_backreferences( dst, 0 );
	}
      else
	{
	  da__fix_backreferences( dst, dst_start );
	}
    }
#endif
}
 
#endif // #ifndef __DATAMAN_NO_DA

//==============================================================================
//    hash table 2
//==============================================================================

#ifndef __DATAMAN_NO_HT

hash_t ht__gen_hash( const void * restrict data, size_t size )
{
  hash_t hash = 65599;

  const char * str = data;
  data = str + size;

  while( str != data )
    {
      hash = (*str) + (hash << 6) + (hash << 16) - hash;
      str++;
    }

  return hash;
}

static bool ht__bin_find( hash_t find_hash, const hash_t * restrict list, index_t list_count, index_t * restrict index )
{
  *index = 0;

  if( list_count == 0 )
    {
      return false;
    }
  
  index_t min = 0;
  index_t max = list_count - 1;

  while( min <= max && max < list_count )
    {
      *index = (min + max) / 2;

      if( find_hash < list[*index] )
	{
	  max = *index - 1;
	}
      else if( find_hash > list[*index] )
	{
	  min = *index + 1;
	}
      else
	{
	  return true;
	}
    }

  if( list[*index] < find_hash )
    {
      (*index)++;
    }

  return false;
}

static bool ht__brute_find( hash_t find_hash, const hash_t * restrict list, index_t list_count, index_t * restrict index )
{
  for( *index = 0; *index < list_count; (*index)++ )
    {
      if( list[*index] == find_hash )
	{
	  return true;
	}
    }
  
  return false;
}

void ht__insert( struct hash_table * table, index_t index, hash_t hash )
{
  pa__insert( &table->pa, index )->uint = hash;
}

bool ht__find( const struct hash_table * table, hash_t find_hash, index_t * restrict index ) // true if found, if not found then the index will be a suitable position to add a new member
{
  if( table->is_disordered )
    {
      return ht__brute_find( find_hash, (void*)table->pa.entry, table->pa.info.count, index );
    }
  else
    {
      return ht__bin_find( find_hash, (void*)table->pa.entry, table->pa.info.count, index );
    }
}

bool ht__add( struct hash_table * table, hash_t add_hash, bool dupe, index_t * restrict index ) // true if added new
{
  if( ht__find( table, add_hash, index ) && dupe )
    {
      return false;
    }
  else
    {
      ht__insert( table, *index, add_hash );

      return true;
    }
}

void ht__clear( struct hash_table * table, bool free_alloc )
{
  pa__clear( &table->pa, free_alloc, NULL );
}

bool ht__delete( struct hash_table * table, index_t index )
{
  return pa__delete( &table->pa, index, true );
}

#endif // #ifndef __DATAMAN_NO_HT

//==============================================================================
//    data table
//==============================================================================

#ifndef __DATAMAN_NO_DT

dab * dt__insert( struct data_table * table, index_t index, hash_t hash )
{
  ht__insert( &table->ht, index, hash );
  table->entry = array_insert_shift( table->entry, index, table->ht.pa.info.count, sizeof( *table->entry ), table->ht.pa.info.flags );
  return table->entry + index;
}

dab * dt__add( struct data_table * table, hash_t hash, enum dt_action action )
{
  index_t index;

  if( ht__find( &table->ht, hash, &index ) )
    {
      if( action == DT__NEW )
	{
	  return NULL;
	}
      else if( action == DT__OVER )
	{
	  return table->entry + index;
	}
      else // dupe
	{
	  return dt__insert( table, index, hash );
	}
    }
  else
    {
      dab * entry = dt__insert( table, index, hash );

      if( action == DT__OVER )
	{
	  *entry = (dab){};
	}
      
      return entry;
    }
}

dab * dt__find( const struct data_table * table, hash_t hash )
{
  index_t find_index;

  if( ht__find( &table->ht, hash, &find_index ) )
    {
      return table->entry + find_index;
    }
  else
    {
      return NULL;
    }
}

index_t dt__index( struct data_table * table, dab * entry )
{
  return entry != NULL ? entry - table->entry : INVALID_INDEX;
}

void dt__clear( struct data_table * table, bool free_alloc, bool free_data )
{
  if( free_data )
    {
      dab * restrict entry;
      
      for( index_t i = 0; i < table->ht.pa.info.count; i++ )
	{
	  entry = table->entry + i;

	  if( entry->free != NULL )
	    {
	      entry->free( entry->data );
	    }
	}
    }
  
  ht__clear( &table->ht, free_alloc );

  if( free_alloc )
    {
      free( table->entry );
      table->entry = NULL;
    }
}

bool dt__delete( struct data_table * table, index_t index, bool free_data )
{
  if( ht__delete( &table->ht, index ) )
    {
      dab * restrict entry = table->entry + index;
      if( free_data && entry->free )
	{
	  entry->free( entry->data );
	}
      ARRAY_SHIFT_DELETE_AFTER( table->entry, table->ht.pa.info.count, index );
      return true;
    }
  else
    {
      return false;
    }
}

#endif // #ifndef __DATAMAN_NO_DT

//==============================================================================
//    dictionary
//==============================================================================

#ifndef __DATAMAN_NO_DICT

hash_t dict__string_hash( const char * str )
{
  return ht__gen_hash( str, strlen( str ) );
}

dab * dict__insert( struct dict * dict, const char * name, hash_t name_hash, index_t index )
{
  dab * ret = dt__insert( &dict->dt, index, name_hash );
  dict->name = array_insert_shift( dict->name, index, dict->dt.ht.pa.info.count, sizeof( *dict->name ), dict->dt.ht.pa.info.flags );
  dict->name[index] = strdup( name );
  return ret;
}

//    intended use: dict__add( dict, name )->content = (struct dict_content){ .data = data, .type = type }
//    inserts an entry into the dictionary, if entries collide, the new entry is inserted after the collided entry
dab *  dict__add( struct dict * dict, const char * name )
{
  hash_t name_hash = dict__string_hash( name );

  index_t index;

  if( ht__find( &dict->dt.ht, name_hash, &index ) && strcmp( name, dict->name[index] ) == 0 )
    {
      return dict->dt.entry + index;
    }
  else
    {
      return dict__insert( dict, name, name_hash, index );
    }
}

dab * dict__find( const struct dict * dict, const char * restrict name )
{
  hash_t hash = dict__string_hash( name );
  index_t index;
  
  if( ht__find( &dict->dt.ht, hash, &index ) )
    {
      return dict->dt.entry + index;
    }
  else
    {
      return NULL;
    }
}

void dict__clear( struct dict * dict, bool free_alloc, bool free_data )
{
  if( free_alloc )
    {
      free( dict->name );
      dict->name = NULL;
    }
  
  dt__clear( &dict->dt, free_alloc, free_data );
}

bool dict__delete( struct dict * dict, index_t index, bool free_data )
{
  if( dt__delete( &dict->dt, index, free_data ) )
    {
      ARRAY_SHIFT_DELETE_AFTER( dict->name, dict->dt.ht.pa.info.count, index );
      return true;
    }
  else
    {
      return false;
    }
}

#endif // #ifndef __DATAMAN_NO_DICT

//==============================================================================
//    Double linked list 
//==============================================================================

#ifndef __DATAMAN_NO_LIST2

void list2__unlink( void * target_member_p, bool free_target_member )
{
  if( target_member_p == NULL )
    {
      return;
    }
  
  struct list2_member * target_member = target_member_p;
  
  struct list2 * target = target_member->link.parent;

  if( target == NULL )
    {
      goto ZERO_MEMBER;
    }

  struct list2_member * next = target_member->link.next;
  struct list2_member * previous = target_member->link.previous;

  // unlink
  
  if( next != NULL )
    {
      next->link.previous = previous;
    }

  if( previous != NULL )
    {
      previous->link.next = next;
    }

  if( target_member == target->first )
    {
      target->first = next;
    }

  if( target_member == target->last )
    {
      target->last = previous;
    }
  
  target->count -= 1;

 ZERO_MEMBER:
  
  if( free_target_member && target_member->info.free )
    {
      target_member->info.free( target_member );
    }
  else
    {
      target_member->link.next = target_member->link.previous = NULL;
      target_member->link.parent = NULL;
    }
}

void list2__clear( struct list2 * target, bool free_data )
{
  if( free_data )
    {
      struct list2_member * member;
      
      list2__FOR_EACH_LINK( *target, member )
	{
	  if( member->info.free )
	    {
	      member->info.free( member );
	    }
	}
    }
  
  da__clear( &target->parking, true, true );

  list2__unlink( target, false );
  
  *target = (struct list2){};
}

void list2__park( struct list2 * target, size_t size, void * target_member_p )
{
  struct list2_member * target_member = target_member_p;

  list2__unlink( target_member, false );
  *da__push( &target->parking ) = (dab){ .data = target_member, .size = size };
}

void * list2__new( struct list2 * target, size_t size )
{
  dab * entry;
  
  for( index_t i = 0; i < target->parking.info.count; i++ )
    {
      entry = target->parking.entry + i;
      if( entry->size == size )
	{
	  void * ret = entry->data;
	  da__delete( &target->parking, i, false, false );
	  return ret;
	}
    }

  return malloc( size );
}

#define list__ADD_FIRST( target, member )	\
  (target).first = (target).last = member;	\
  ((struct list2_member*)member)->link.parent = &(target);	\
  (target).count = 1;

struct list2_member * list2__add_after( struct list2 * target, void * target_member_p, void * compatible_struct )
{
  if( target->count == 0 )
    {
      list__ADD_FIRST( *target, compatible_struct );
      return target->last;
    }
      
  if( target_member_p == NULL )
    {
      target_member_p = target->last;
    }
  
  struct list2_member * target_member = target_member_p;
  struct list2_member * add = compatible_struct;
  struct list2_member * old_next = target_member->link.next;

  target_member->link.next = add;
  
  if( old_next != NULL )
    {
      old_next->link.previous = add;
    }
  else
    {
      target->last = add;
    }

  *add = (struct list2_member){ { target_member, old_next, target }, { NULL, 0 } };
  
  target->count++;

  return add;
}

struct list2_member * list2__add_before( struct list2 * target, void * target_member_p, void * compatible_struct )
{
  if( target->count == 0 )
    {
      list__ADD_FIRST( *target, compatible_struct );
      return target->first;
    }
      
  if( target_member_p == NULL )
    {
      target_member_p = target->first;
    }
  
  struct list2_member * target_member = target_member_p;
  struct list2_member * add = compatible_struct;
  struct list2_member * old_previous = target_member->link.previous;

  target_member->link.previous = add;
  
  if( old_previous != NULL )
    {
      old_previous->link.next = add;
    }
  else
    {
      target->first = add;
    }

  *add = (struct list2_member){ { old_previous, target_member, target }, { NULL, TYPE__NOT_SET } };
  
  target->count++;

  return add;
}

struct list2_member * list2__append( struct list2 * target, void * compatible_struct )
{
  if( target->count == 0 )
    {
      list__ADD_FIRST( *target, compatible_struct );
    }
  else
    {
      list2__add_after( target, NULL, compatible_struct );
    }
  
  return target->first;
}

struct list2_member * list2__prepend( struct list2 * target, void * compatible_struct )
{
  if( target->count == 0 )
    {
      list__ADD_FIRST( *target, compatible_struct );
    }
  else
    {
      list2__add_before( target, NULL, compatible_struct );
    }
  
  return target->first;
}

#endif // #ifndef __DATAMAN_NO_LIST2

//==============================================================================
