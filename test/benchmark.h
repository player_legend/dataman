//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    benchmark.h

    Benchmarking tools
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BENCHMARK_H
#define BENCHMARK_H

//==============================================================================
//    Includes
//==============================================================================

//
//    System
//

#include <sys/time.h>
#include <stdio.h>

//==============================================================================
//    Definitions
//==============================================================================

#define US_MAX (unsigned long long)1e6

typedef unsigned long long bm_count_t;

struct benchmark
{
  const char * name;
  struct timeval tv_start;
  struct timeval tv_end;
  time_t s_total;
  suseconds_t us_total;
  bm_count_t count;
};

static struct benchmark bm__new( const char * name )
{
  return (struct benchmark){ .name = name };
}

static void bm__internal_tally_time( struct benchmark * bm )
{
  long long us_diff = bm->tv_end.tv_usec - bm->tv_start.tv_usec + US_MAX*(bm->tv_end.tv_sec - bm->tv_start.tv_sec);
  
  bm->s_total += (bm->us_total + us_diff) / US_MAX;
  bm->us_total = (bm->us_total + us_diff) % US_MAX;
  
  bm->count++;
}

#define bm__START( benchmark ) gettimeofday( &(benchmark).tv_start, NULL );

#define bm__END( benchmark ) gettimeofday( &(benchmark).tv_end, NULL ); bm__internal_tally_time( &(benchmark) );

static void bm__print_stats( struct benchmark * bm, double frametime, double max_frame_use )
{
  double us_average = (double)(bm->us_total + US_MAX*bm->s_total) / (double)bm->count;
  double max_us = US_MAX * frametime * max_frame_use;
  
  printf( "Benchmark %s -- count %llu\n", bm->name, bm->count );
  printf( "\t%eus average\n", (double)us_average );
  printf( "\t%eus total\n", (double) (bm->us_total + US_MAX * bm->s_total) );
  //printf( "\t%es total\n", ((double)bm->s_total / (double)US_MAX) + (double)bm->s_total );
  printf( "\t%e percent of target frametime\n", (double)us_average / ((double) US_MAX * frametime ) );
  printf( "\t%f can be used per frame while remaining on time.\n", (double)max_us / (double)us_average );
}

//==============================================================================

#endif // #ifndef BENCHMARK_H
