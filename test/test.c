//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    test.c

    Tests for the dataman library
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
  Copyright 2018 Joshua D. Sawyer

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "dataman.h"
#include "benchmark.h"
#include <stdio.h>

const bm_count_t test_size = 1e6;
const bm_count_t test_div = 1e2;

void print_index_pa( struct ptr_array * pa )
{
  for( index_t i = 0; i < pa->info.count; i++ )
    {
      printf("\t%lu\n", pa->entry[i].uint );
      fflush(stdout);
    }
}

void test_pa_push()
{
  printf("test pa_push\n");

  struct ptr_array pa = {};
  
  index_t i = 0;
  
  while( i < test_size )
    {
      pa__push( &pa )->uint = i++;
    }

  print_index_pa( &pa );
  
  pa__clear( &pa, true, NULL );
}

void test_pa_pop()
{
  printf("test pa_pop\n");

  struct ptr_array pa = {};
  
  index_t i = 0;
  
  while( i < test_size )
    {
      pa__push( &pa )->uint = i++;
    }

  while( pa.info.count > 0 )
    {
      printf("\t%lu\n", pa__pop( &pa ).uint );
    }
  print_index_pa( &pa );
  
  pa__clear( &pa, true, NULL );
}

void test_pa_add()
{
  printf("test pa_add\n");
  
  struct ptr_array array = {};
  
  index_t i = 0;

  //pa__add( &array, test_size )->uint = 696969696;
  while( i < test_size )
    {
      //pa__insert( &array, rand() % test_size )->uint = 3;
      pa__add( &array, rand() % test_size )->uint = 3;
      i++;
    }

  print_index_pa( &array );

  pa__clear( &array, true, NULL );
}

void print_index_da( struct data_array * da )
{
  for( index_t i = 0; i < da->info.count; i++ )
    {
      printf("\t%lu\n", da->entry[i].uint );
      fflush(stdout);
    }
}

void test_da_insert()
{
  struct data_array da = {};

  *da__push( &da ) = (dab){ .uint = 2 };
  *da__push( &da ) = (dab){ .uint = 2 };
  *da__push( &da ) = (dab){ .uint = 2 };
  *da__push( &da ) = (dab){ .uint = 1 };
  *da__push( &da ) = (dab){ .uint = 1 };
  *da__push( &da ) = (dab){ .uint = 1 };
  *da__push( &da ) = (dab){ .uint = 1 };
  *da__insert( &da, 0 ) = (dab){ .uint = 3 };
  *da__insert( &da, 0 ) = (dab){ .uint = 3 };
  *da__insert( &da, 0 ) = (dab){ .uint = 3 };

  print_index_da( &da );
}

void test_da_pop()
{
  printf("test da_pop\n");

  struct data_array da = {};
  
  index_t i = 0;
  
  while( i < test_size )
    {
      da__push( &da )->uint = i++;
    }

  while( da.info.count > 0 )
    {
      printf("\t%lu\n", da__pop( &da ).uint );
    }
  
  print_index_da( &da );
  
  da__clear( &da, true, NULL );
}

void test_da_add()
{
  printf("test da_add\n");
  
  struct data_array array = {};
  
  index_t i = 0;

  //da__add( &array, test_size )->uint = 696969696;
  while( i < test_size )
    {
      //da__insert( &array, rand() % test_size )->uint = 3;
      *da__add( &array, rand() % test_size ) = (dab){ .uint = 3 };
      i++;
    }

  print_index_da( &array );

  da__clear( &array, true, NULL );
}

void print_dict( struct dict * dict )
{
  for( index_t i = 0; i < dict->dt.ht.pa.info.count; i++ )
    {
      printf( "\t%8s : %lu\n", dict->name[i], dict->dt.entry[i].uint );
      fflush(stdout);
    }
}

void random_word( char * dest, size_t len )
{
  dest[len] = '\0';
  do
    {
      len--;
      dest[len] = (rand() & 1 ? 'a' : 'A') + (rand() % 26);
    }
  while( len != 0 );
}

void test_dict_add()
{
  struct dict dict = {};

  dict__add( &dict, "ass" )->uint = 1;
  dict__add( &dict, "butter" )->uint = 2;
  dict__add( &dict, "fart" )->uint = 3;
  
  for( index_t i = 0; i < test_size; i++ )
    {
    }

  print_dict( &dict );

  dict__clear( &dict, true, false );
}

void benchmark_da_insert()
{
  struct data_array da = {};

  struct benchmark bm = bm__new( "da_insert" );

  index_t random;
  da__add( &da, test_div );

  printf("test array size %llu\n", test_div);
  
  for( bm_count_t i = 0; i < test_size; i++ )
    {
      random = rand() % test_div;
      bm__START( bm );
      da__insert( &da, random );
      bm__END( bm );
      da__pop( &da );
    }

  bm__print_stats( &bm, 1.0 / 60.0, 0.05 );
}

void benchmark_da_push()
{
  struct data_array da = {};

  struct benchmark bm = bm__new( "da_push" );

  for( bm_count_t i = 0; i < test_size; i++ )
    {
      bm__START( bm );
      da__push( &da );
      bm__END( bm );
    }

  bm__print_stats( &bm, 1.0 / 60.0, 0.05 );
}

void benchmark_pa_insert()
{
  struct ptr_array pa = {};

  struct benchmark bm = bm__new( "pa_insert" );

  index_t random;
  pa__add( &pa, test_div );

  printf("test array size %llu\n", test_div);
  
  for( bm_count_t i = 0; i < test_size; i++ )
    {
      random = rand() % test_div;
      bm__START( bm );
      pa__insert( &pa, random );
      bm__END( bm );
      pa__pop( &pa );
    }

  bm__print_stats( &bm, 1.0 / 60.0, 0.05 );
}

void benchmark_pa_push()
{
  struct ptr_array pa = {};

  struct benchmark bm = bm__new( "pa_push" );

  for( bm_count_t i = 0; i < test_size; i++ )
    {
      bm__START( bm );
      pa__push( &pa );
      bm__END( bm );
    }

  bm__print_stats( &bm, 1.0 / 60.0, 0.05 );
}

void benchmark_dict_add()
{
  struct dict dict = {};

  size_t word_len = 8;
  char word[word_len + 1];

  struct benchmark bm = bm__new( "dict_add" );

  for( bm_count_t i = 0; i < test_size; i++ )
    {
      random_word( word, word_len );
      //printf("add %s\n", word);
      bm__START( bm );
      dict__add( &dict, word );
      bm__END( bm );

      if( i % test_div == 0 )
	{
	  dict__clear( &dict, true, false );
	}
    }

  bm__print_stats( &bm, 1.0 / 60.0, 0.05 );
}

enum tree_action print_tree_leaf( node_arg arg )
{
  printf( "%.*s%lu -- %d/%d\n", arg.pos.depth, "\t\t\t\t\t\t\t\t\t\t\t", arg.ptr.data->uint, arg.pos.index, arg.pos.parent_count );

  return TA__CONT;
}

enum tree_action print_tree_leaf2( node_arg arg )
{
  //printf( "%.*s%lu -- %d/%d %p parent %p\n", arg.depth, "\t\t\t\t\t\t\t\t\t\t\t", arg.data->uint, arg.index, arg.parent_count, arg.data, arg.parent );
  printf( "%.*s%lu -- %d/%d -- %s\n", arg.pos.depth, "\t\t\t\t\t\t\t\t\t\t\t", arg.ptr.data->uint, arg.pos.index, arg.pos.parent_count, arg.is_new ? "new" : "old" );
  if( arg.is_new )
    {
      static int i = 0;
      arg.ptr.data->uint = i++;
    }

  if( arg.pos.depth == 1 )
    {
      return TA__NODE_PARENT;
    }
  else if( arg.pos.parent_count < 8 )
    {
      return TA__DATA_NEXT;
    }
  else
    {
      return TA__CONT;
    }
}

enum tree_action print_tree_node( node_arg arg )
{
  printf( "%.*snode-%s %d/%d -- %p node %p parent\n", arg.pos.depth, "\t\t\t\t\t\t\t\t\t\t\t", arg.parent_pre ? "pre" : "post", arg.pos.index, arg.pos.parent_count, arg.ptr.node, arg.ptr.parent );
 return TA__CONT;
}

void print_tree( struct data_array * root )
{
  da__walk( root, &(da_walk_options){ .pre_node_func = print_tree_node, .leaf_func = print_tree_leaf, .post_node_func = print_tree_node, .reverse = false } );
}

void print_tree2( struct data_array * root )
{
  da__walk( root, &(da_walk_options){ .pre_node_func = print_tree_node, .leaf_func = print_tree_leaf2, .post_node_func = print_tree_node, .reverse = false } );
}

enum tree_action print_tree_node2( node_arg arg )
{
  return TA__RAISE_CHILDREN;
}

void print_tree3( struct data_array * root )
{
  da__walk( root, &(da_walk_options){ .pre_node_func = print_tree_node2, .leaf_func = print_tree_leaf, .post_node_func = print_tree_node2, .reverse = false } );
}

void test_tree_walk2()
{
  struct data_array da = {};
  *da__push( &da ) = (dab){ .uint = 111 };
  *da__push( &da ) = (dab){ .uint = 222 };
  *da__push( &da ) = (dab){ .uint = 333 };
  *da__push( &da ) = (dab){ .uint = 444 };
  
  printf("n==============================================================================\n\n");
  
  print_tree( &da );

  printf("2==============================================================================\n\n");
  
  print_tree2( &da );

  printf("2==============================================================================\n\n");
  
  print_tree2( &da );
  
  printf("n==============================================================================\n\n");
  
  print_tree( &da );
  
  printf("3==============================================================================\n\n");
  
  print_tree3( &da );
  
  printf("n==============================================================================\n\n");
  
  print_tree( &da );
}

void test_da_copy()
{
  struct data_array
    da1 = {},
    da2 = {};

  *da__push( &da1 ) = (dab){ .uint = 0 };
  *da__push( &da1 ) = (dab){ .uint = 1 };
  *da__push( &da1 ) = (dab){ .uint = 2 };

  *da__push( &da2 ) = (dab){ .uint = 4 };
  *da__push( &da2 ) = (dab){ .uint = 5 };
  *da__push( &da2 ) = (dab){ .uint = 6 };
  *da__push( &da2 ) = (dab){ .uint = 7 };
  *da__push( &da2 ) = (dab){ .uint = 8 };
  *da__push( &da2 ) = (dab){ .uint = 9 };
  *da__push( &da2 ) = (dab){ .uint = 10 };

  //da__copy_range( &da2, &da1, 3, 0, 3, 1 );

  print_index_da( &da2 );
}

void test_tree_walk()
{
  struct data_array da = {};

  da__push( &da );
  struct data_array * node1 = da__make_node( &da, 0 );
  {
    da__push( node1 );
    struct data_array * node2 = da__make_node( node1, 0 );
    *da__push( node2 ) = (dab){};
    *da__push( node2 ) = (dab){};
    *da__push( node2 ) = (dab){};
    *da__push( node2 ) = (dab){};
  }
  
  *da__push( node1 ) = (dab){};
  *da__push( node1 ) = (dab){ .uint = 3 };
  *da__push( node1 ) = (dab){};

  for( index_t i = 0; i < 10; i++ )
    {
      *da__push( &da ) = (dab){};
    }

  printf( "Call children %u\n", da.info.count );
  
  print_tree( &da );
}

int main()
{
  printf("da:  %lu -> %lu\n", sizeof( struct data_array ), sizeof( struct data_array ) / sizeof( void* ) );
  printf("dab: %lu -> %lu\n", sizeof( dab ), sizeof( dab ) / sizeof( void* ) );
  printf("pa:  %lu -> %lu\n", sizeof( struct ptr_array ), sizeof( struct ptr_array ) / sizeof( void* ) );

  
  //test_da_pop();
  
  //test_da_add();
  
  //test_dict_add();
  
  //test_pa_pop();
  
  //test_pa_add();

  //test_pa_push();

  test_tree_walk2();

  //test_da_copy();

  //test_da_insert();
  
  //benchmark_da_push();

  //benchmark_da_insert();

  //benchmark_pa_push();

  //benchmark_pa_insert();

  //benchmark_dict_add();
}
